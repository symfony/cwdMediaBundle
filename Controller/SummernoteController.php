<?php
/*
 * This file is part of CwdMediaBundle
 *
 * (c)2016 Ludwig Ruderstaller <lr@cwd.at>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Cwd\MediaBundle\Controller;

use Cwd\MediaBundle\Service\MediaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SummernoteController
 *
 * @package Cwd\MediaBundle\Controller
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @Route("/media")
 */
class SummernoteController extends Controller
{
    /**
     * @Route("/upload")
     *
     * @param Request $request
     * @return mixed
     * @throws \Cwd\MediaBundle\MediaException
     */
    public function uploadAction(Request $request)
    {
        /** @var MediaService $service */
        $service = $this->get('cwd.media.service');

        $media = $service->create($request->files->get('file'), true);
        $service->flush();
        $image = $service->createInstance($media)->cropResize(1000);

        $url = sprintf(
            "%s?mediaId=%d&width=%d&height=%d",
            $image->__toString(),
            $media->getId(),
            $image->width(),
            $image->height()
        );

        $data = array(
            'url' => $url,
        );

        return JsonResponse::create($data);
    }
}
