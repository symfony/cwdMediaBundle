<?php
/*
 * This file is part of CwdMediaBundle
 *
 * (c)2016 Ludwig Ruderstaller <lr@cwd.at>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Cwd\MediaBundle\Controller;

use Cwd\MediaBundle\Model\Entity\Media;
use Cwd\MediaBundle\Service\MediaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MediaController
 *
 * @package Cwd\MediaBundle\Controller
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @Route("/media")
 */
class MediaController extends Controller
{
    /**
     * @param Media $media
     * @Route("/file/{media}")
     * @ParamConverter("media", class="Model:Media", options={"mapping": {"media" = "id"}})
     * @return BinaryFileResponse
     */
    public function fileAction(Media $media)
    {
        return new BinaryFileResponse($this->get('cwd.media.service')->getFilePath($media));
    }
}
