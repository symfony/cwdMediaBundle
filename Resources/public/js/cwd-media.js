var CwdMedia = function () {

    return {
        data: {
            summernoteImageUploadPath: ''
        },
        /**
         * calls all required init functions
         */
        init: function () {
            CwdMedia.initSummernote();
        },
        /**
         *
         */
        initSummernote: function() {
            $('.summernote').each(function() {
                var $this = $(this);
                $this.summernote({
                    height: 400,
                    onImageUpload: function(files, editor, welEditable) {
                        CwdMedia.sendFile(files[0], $this);
                    }
                });
            });
        },
        /**
         *
         */
        sendFile: function sendFile(file, $editor) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: CwdMedia.data.summernoteImageUploadPath,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {
                    $editor.summernote('insertImage', data.url);
                }
            });
        }
    };
}();