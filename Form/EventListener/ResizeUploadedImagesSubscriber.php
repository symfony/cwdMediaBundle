<?php
/*
 * This file is part of CwdMediaBundle
 *
 * (c)2016 Ludwig Ruderstaller <lr@cwd.at>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Cwd\MediaBundle\Form\EventListener;

use Cwd\MediaBundle\Service\MediaService;
use Proxies\__CG__\Zitate\Model\Entity\Media;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ResizeUploadedImagesSubscriber
 * @package Cwd\MediaBundle\Form\EventListener
 */
class ResizeUploadedImagesSubscriber implements EventSubscriberInterface
{

    /**
     * @var MediaService
     */
    protected $mediaService;

    /**
     * ResizeUploadedImagesSubscriber constructor.
     * @param MediaService $mediaService
     */
    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SUBMIT => 'preSubmit');
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $content = $event->getData();

        if ($content !== null) {
            $pattern = '/<img\s[^>]*?src\s*=\s*[\'\"]([^\'\"]*?)\?mediaId=([0-9]*)\&amp;width=([0-9]*)\&amp;height=([0-9]*)[\'\"][^>]*?>/';
            preg_match_all($pattern, $content, $matches);

            foreach ($matches[0] as $index => $imgTag) {
                $mediaId = intval($matches[2][$index]);
                $origWidth = intval($matches[3][$index]);
                $origHeight = intval($matches[4][$index]);
                $oldUrl = $matches[1][$index];

                $pattern = '/style\s*=\s*[\'\"]([^\'\"]*?)[\'\"]/';
                preg_match($pattern, $imgTag, $styleMatches);

                if (sizeof($styleMatches) == 2) {
                    $media = $this->mediaService->find($mediaId);
                    $image = $this->mediaService->createInstance($media);
                    $newImgTag = $imgTag;

                    $width = null;
                    $height = null;

                    $widthPattern = '/width:\s*(\d+|\d*\.\d+)(px|%);/';
                    preg_match($widthPattern, $styleMatches[1], $widthMatches);
                    if (sizeof($widthMatches) == 3) {
                        $width = doubleval($widthMatches[1]);
                        if ($widthMatches[2] == '%') {
                            $width = ($origWidth / 100) * $width;
                        }

                        $newImgTag = str_replace($widthMatches[0], '', $newImgTag);
                    }

                    $heightPattern = '/height:\s*(\d+|\d*\.\d+)(px|%);/';
                    preg_match($heightPattern, $styleMatches[1], $heightMatches);
                    if (sizeof($heightMatches) == 3) {
                        $height = doubleval($heightMatches[1]);
                        if ($heightMatches[2] == '%') {
                            $height = ($origWidth / 100) * $height;
                        }
                        $newImgTag = str_replace($heightMatches[0], '', $newImgTag);
                    }

                    $image->cropResize($width, $height);

                    $newUrl = $image->__toString();
                    $newImgTag = str_replace($oldUrl, $newUrl, $newImgTag);

                    if ($width === null) {
                        $width = 0;
                    }
                    if ($height === null) {
                        $height = 0;
                    }
                    $newImgTag = str_replace('width='.$origWidth, 'width='.intval($width), $newImgTag);
                    $newImgTag = str_replace('height='.$origHeight, 'height='.intval($height), $newImgTag);

                    $content = str_replace($imgTag, $newImgTag, $content);
                }
            }
            $event->setData($content);
        }
    }
}
