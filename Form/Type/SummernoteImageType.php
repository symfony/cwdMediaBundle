<?php
/*
 * This file is part of cwd media bundle.
 *
 * (c)2015 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\MediaBundle\Form\Type;

use Cwd\MediaBundle\Form\EventListener\ResizeUploadedImagesSubscriber;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SummernoteImageType
 * @package Cwd\MediaBundle\Form\Type
 */
class SummernoteImageType extends TextareaType
{
    /**
     * @var ResizeUploadedImagesSubscriber
     */
    protected $subscriber;

    /**
     * SummernoteImageType constructor.
     * @param ResizeUploadedImagesSubscriber $subscriber
     */
    public function __construct(ResizeUploadedImagesSubscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventSubscriber($this->subscriber);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('attr', array('class' => 'summernote'));
    }
}
