<?php
/*
 * This file is part of cwd media bundle.
 *
 * (c)2015 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\MediaBundle\Form\Type;

use Cwd\MediaBundle\Form\Transformer\MediaTransformer;
use Cwd\MediaBundle\Model\Entity\Media;
use Cwd\MediaBundle\Service\MediaService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ImageType
 *
 * @package Cwd\MediaBundle\Form\Type
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class ImageType extends AbstractType
{
    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * @param MediaService $mediaService
     */
    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $media = $event->getData();
                if ($media instanceof Media) {
                    return $media;
                } elseif (is_numeric($media)) {
                    $event->setData($this->mediaService->find($media));
                } elseif ($media instanceof UploadedFile) {
                    $event->setData($this->mediaService->create($media->getPathname(), true));
                }
            }
        );

        parent::buildForm($builder, $options);
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['multiple']) {
            $view->vars['full_name'] .= '[]';
            $view->vars['attr']['multiple'] = 'multiple';
        }

        $view->vars = array_replace($view->vars, array(
            'type' => 'file',
            'value' => '',
        ));
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array $options
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multipart'] = true;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $emptyData = function (Options $options) {
            return $options['multiple'] ? array() : null;
        };

        $resolver->setDefaults(array(
            'compound' => false,
            'data_class'            => 'Cwd\MediaBundle\Model\Entity\Media',
            'empty_data' => $emptyData,
            'multiple' => false,
            'attr' => ['class' => 'image-upload'],
            'validation_groups'     => array('default'),
            'cascade_validation'    => true,
            'allow_file_upload'     => true,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cwd_image_type';
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return 'cwd_image_type';
    }
}
